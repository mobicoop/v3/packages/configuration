## Demande de fusion de nouvelle version

## Liste de vérifications

### Demande de fusion

- [ ] La branche cible est identifiée.
- [ ] La documentation reflète les changements effectués (Changelog généré).
- [ ] Les tests passent dans le pipeline gitlab et en local.

### Gestion du changegement

- [ ] La version est tagguée.
