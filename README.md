# Mobicoop V3 - Configuration package

NPM package used to get / set configuration items for Mobicoop V3 services into a Redis database.

## Requirements

-   a running Redis server

## Installation

```bash
npm install --save @mobicoop/configuration-module
```

## Usage

Add the module in the _imports_ section of the parent module, for example, using NestJs ConfigModule to inject values from `.env`:

```ts
...
import {
  ConfigurationModule,
  ConfigurationModuleOptions,
} from '@mobicoop/configuration-module';
import { ConfigModule, ConfigService } from '@nestjs/config';
...
imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    ConfigurationModule.forRootAsync({
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: async (
        configService: ConfigService,
        ): Promise<ConfigurationModuleOptions> => ({
            host: configService.get<string>('REDIS_HOST'),
            password: configService.get<string>('REDIS_PASSWORD'),
            port: configService.get<number>('REDIS_PORT'),
        }),
    }),
],
...
```

You need to set the following options :

-   `host` : the redis hostname
-   `password` : the redis password
-   `port` : the redis port

Then you need to inject the ConfigurationRepository in a service with the appropriate port (`GetConfigurationRepositoryPort` to get a value, `SetConfigurationRepositoryPort` to set a value) :

```ts
...
import {
  ConfigurationDomain,
  ConfigurationType,
  ConfigurationValue,
  GetConfigurationRepositoryPort,
} from '@mobicoop/configuration-module';
...
constructor(
    @Inject(CONFIGURATION_REPOSITORY)
    private readonly configurationRepository: GetConfigurationRepositoryPort,
  ) {}
...
const myValue: ConfigurationValue = await this.configurationRepository.get(
    {
    domain: ConfigurationDomain.CARPOOL,
    key: 'seatsProposed',
    type: ConfigurationType.INT,
    },
);
...
```

The `get` methods requires an identifier (a domain and a key) as well as as a return type : the value will automatically be casted from string (as Redis stores values as strings) to the required type.

The `mget` can return many items but for a single domain. It requires the domain and a list of keys with their return type. It returns a Configurator object, that contains the list of configuration items (consisting in the domain, a key, and the value associated casted to the required type), as well as a `get` method to get a configuration value by its key. If one of the identifiers is not found, the whole methods throws.

The `set` methods requires an identifier (a domain and a key) and a value. The value will be casted to string (as Redis stores values as strings).

## Available domains

-   **AUTH** : authentication & authorization related configuration items (eg. default password encryption algorithm)
-   **CARPOOL** : carpool related configuration items (eg. default number of seats proposed as a driver)
-   **GEOGRAPHY** : geographic related configuration items (eg. georouter settings)
-   **MATCH** : matching related configuration items (eg. default algorithm type)
-   **PAGINATION** : pagination related configuration items (eg. default number of results per page)

New domains will be added in the future depending on the needs !

## Available types

As Redis stores values as strings, we need to cast these values when we get them. The available types are :

-   BOOLEAN
-   INT
-   FLOAT
-   JSON
-   JSON_ARRAY
-   STRING
-   STRING_ARRAY (stored in redis as a comma-separated string)
-   INT_ARRAY (stored in redis as a comma-separated string)
-   FLOAT_ARRAY (stored in redis as a comma-separated string)
