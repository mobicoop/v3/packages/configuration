import { Test, TestingModule } from '@nestjs/testing';
import { getRedisToken } from '@songkeys/nestjs-redis';
import { ConfigurationRepository } from '../../configuration.repository';
import {
  Domain,
  KeyType,
  Identifier,
  Type,
} from '../../configuration.repository.port';
import { NotFoundException } from '../../configuration.exception';
import { Configurator } from '../../configurator';

const mockRedis = {
  get: jest
    .fn()
    .mockImplementationOnce(() => '1')
    .mockImplementationOnce(() => '0.3')
    .mockImplementationOnce(() => 'passenger')
    .mockImplementationOnce(() => 'false')
    .mockImplementationOnce(() => '1,2,10')
    .mockImplementationOnce(() => '1.5,2.4,10.2')
    .mockImplementationOnce(() => 'driver,passenger,walker,cyclist')
    .mockImplementationOnce(() => '{"foo":"bar"}')
    .mockImplementationOnce(
      () => '[{"foo":"bar"},{"lorem":"ipsum","dolor":{"sit":"amet"}}]',
    )
    .mockImplementation(() => null),
  mget: jest
    .fn()
    .mockImplementationOnce(() => [
      '1',
      '0.3',
      'passenger',
      'false',
      '1,2,10',
      '1.5,2.4,10.2',
      'driver,passenger,walker,cyclist',
      '{"foo":"bar"}',
      '[{"foo":"bar"},{"lorem":"ipsum","dolor":{"sit":"amet"}}]',
    ])
    .mockImplementationOnce(() => ['1', '0.3', null, 'false']),
  set: jest.fn().mockImplementation(),
};

describe('Configuration Repository', () => {
  let configurationRepository: ConfigurationRepository;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: getRedisToken('default'),
          useValue: mockRedis,
        },
        ConfigurationRepository,
      ],
    }).compile();

    configurationRepository = module.get<ConfigurationRepository>(
      ConfigurationRepository,
    );
  });

  it('should be defined', () => {
    expect(configurationRepository).toBeDefined();
  });

  describe('get', () => {
    it('should get a value as int', async () => {
      expect(
        await configurationRepository.get(Domain.CARPOOL, {
          key: 'seatsProposed',
          type: Type.INT,
        }),
      ).toBe(1);
    });
    it('should get a value as float', async () => {
      expect(
        await configurationRepository.get(Domain.CARPOOL, {
          key: 'maxDetourDistanceRatio',
          type: Type.FLOAT,
        }),
      ).toBe(0.3);
    });
    it('should get a value as string', async () => {
      expect(
        await configurationRepository.get(Domain.CARPOOL, {
          key: 'role',
          type: Type.STRING,
        }),
      ).toBe('passenger');
    });
    it('should get a value as boolean', async () => {
      expect(
        await configurationRepository.get(Domain.CARPOOL, {
          key: 'useAzimuth',
          type: Type.BOOLEAN,
        }),
      ).toBeFalsy();
    });
    it('should get a value as int array', async () => {
      expect(
        await configurationRepository.get(Domain.CARPOOL, {
          key: 'testIntArray',
          type: Type.INT_ARRAY,
        }),
      ).toStrictEqual([1, 2, 10]);
    });
    it('should get a value as float array', async () => {
      expect(
        await configurationRepository.get(Domain.CARPOOL, {
          key: 'testFloatArray',
          type: Type.FLOAT_ARRAY,
        }),
      ).toStrictEqual([1.5, 2.4, 10.2]);
    });
    it('should get a value as string array', async () => {
      expect(
        await configurationRepository.get(Domain.CARPOOL, {
          key: 'testStringArray',
          type: Type.STRING_ARRAY,
        }),
      ).toStrictEqual(['driver', 'passenger', 'walker', 'cyclist']);
    });
    it('should get a value as json', async () => {
      expect(
        await configurationRepository.get(Domain.CARPOOL, {
          key: 'testJson',
          type: Type.JSON,
        }),
      ).toStrictEqual({
        foo: 'bar',
      });
    });
    it('should get a value as json array', async () => {
      expect(
        await configurationRepository.get(Domain.CARPOOL, {
          key: 'testJsonArray',
          type: Type.JSON_ARRAY,
        }),
      ).toStrictEqual([
        { foo: 'bar' },
        {
          lorem: 'ipsum',
          dolor: { sit: 'amet' },
        },
      ]);
    });
    it('should throw if configuration is not found', async () => {
      await expect(
        configurationRepository.get(Domain.CARPOOL, {
          key: 'seatsProposed',
          type: Type.INT,
        }),
      ).rejects.toBeInstanceOf(NotFoundException);
    });
  });

  describe('mget', () => {
    it('should get a list of values', async () => {
      const identifiers: KeyType[] = [
        {
          key: 'seatsProposed',
          type: Type.INT,
        },
        {
          key: 'maxDetourDistanceRatio',
          type: Type.FLOAT,
        },
        {
          key: 'role',
          type: Type.STRING,
        },
        {
          key: 'useAzimuth',
          type: Type.BOOLEAN,
        },
        {
          key: 'testIntArray',
          type: Type.INT_ARRAY,
        },
        {
          key: 'testFloatArray',
          type: Type.FLOAT_ARRAY,
        },
        {
          key: 'testStringArray',
          type: Type.STRING_ARRAY,
        },
        {
          key: 'testJson',
          type: Type.JSON,
        },
        {
          key: 'testJsonArray',
          type: Type.JSON_ARRAY,
        },
      ];
      const configurator: Configurator = await configurationRepository.mget(
        Domain.CARPOOL,
        identifiers,
      );
      expect(configurator.configurations).toHaveLength(9);
      expect(configurator.configurations[1].value).toBe(0.3);
      expect(configurator.configurations[5].value).toStrictEqual([
        1.5, 2.4, 10.2,
      ]);
    });
    it('should throw if a configuration item is null', async () => {
      const identifiers: KeyType[] = [
        {
          key: 'seatsProposed',
          type: Type.INT,
        },
        {
          key: 'maxDetourDistanceRatio',
          type: Type.FLOAT,
        },
        {
          key: 'role',
          type: Type.STRING,
        },
        {
          key: 'useAzimuth',
          type: Type.BOOLEAN,
        },
      ];
      await expect(
        configurationRepository.mget(Domain.CARPOOL, identifiers),
      ).rejects.toBeInstanceOf(NotFoundException);
    });
  });

  describe('set', () => {
    it('should set a primitive value', async () => {
      const configurationIdentifier: Identifier =
        await configurationRepository.set(Domain.CARPOOL, 'seatsProposed', 3);
      expect(configurationIdentifier.domain).toBe('CARPOOL');
      expect(configurationIdentifier.key).toBe('seatsProposed');
    });
    it('should set a value as array', async () => {
      const configurationIdentifier: Identifier =
        await configurationRepository.set(
          Domain.CARPOOL,
          'testSetIntArray',
          [3, 2, 5, 12],
        );
      expect(configurationIdentifier.domain).toBe('CARPOOL');
      expect(configurationIdentifier.key).toBe('testSetIntArray');
    });
  });
});
