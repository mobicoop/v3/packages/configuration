import { NotFoundException } from '../../configuration.exception';
import { Configuration, Domain } from '../../configuration.repository.port';
import { Configurator } from '../../configurator';

const configurations: Configuration[] = [
  {
    domain: Domain.CARPOOL,
    key: 'seatsProposed',
    value: 3,
  },
  {
    domain: Domain.CARPOOL,
    key: 'seatsRequested',
    value: 1,
  },
  {
    domain: Domain.CARPOOL,
    key: 'role',
    value: 'passenger',
  },
];

describe('Configurator', () => {
  it('should return a configuration value', () => {
    const configurator: Configurator = new Configurator(
      Domain.CARPOOL,
      configurations,
    );
    expect(configurator.get<number>('seatsRequested')).toBe(1);
  });
  it('should throw if configuration is not found', () => {
    const configurator: Configurator = new Configurator(
      Domain.CARPOOL,
      configurations,
    );
    expect(() => configurator.get<number>('seatsReserved')).toThrow(
      NotFoundException,
    );
  });
});
