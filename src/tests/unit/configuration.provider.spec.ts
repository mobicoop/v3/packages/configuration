import { Provider } from '@nestjs/common';
import { ConfigurationModuleAsyncOptions } from '../../configuration-options.interface';
import { createAsyncOptionsProvider } from '../../configuration.provider';

describe('Configuration provider', () => {
  it('should return an async options provider', async () => {
    const configurationModuleAsyncOptions: ConfigurationModuleAsyncOptions = {
      useFactory: jest.fn(),
      imports: [jest.fn()],
      inject: [jest.fn()],
    };
    const expectedProvider: Provider = createAsyncOptionsProvider(
      configurationModuleAsyncOptions,
    );
    expect(expectedProvider).toBeDefined();
  });
});
