export class NotFoundException extends Error {
  static readonly message = 'Not found';

  constructor(message = NotFoundException.message) {
    super(message);
  }
}
