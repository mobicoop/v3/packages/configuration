import { Injectable } from '@nestjs/common';
import { InjectRedis } from '@songkeys/nestjs-redis';
import { Redis } from 'ioredis';
import {
  Domain,
  KeyType,
  Identifier,
  Key,
  Type,
  Value,
  GetConfigurationRepositoryPort,
  SetConfigurationRepositoryPort,
} from './configuration.repository.port';
import { NotFoundException } from './configuration.exception';
import { Configurator } from './configurator';

@Injectable()
export class ConfigurationRepository
  implements GetConfigurationRepositoryPort, SetConfigurationRepositoryPort
{
  constructor(@InjectRedis() private readonly redis: Redis) {}

  get = async <T>(domain: Domain, keyType: KeyType): Promise<T> => {
    const identifier: string = `${domain}:${keyType.key}`;
    const value: string | null = await this.redis.get(identifier);
    if (!value)
      throw new NotFoundException(
        `Configuration item not found for key ${identifier}`,
      );
    return this._cast(value, keyType.type) as T;
  };

  mget = async (domain: Domain, keyTypes: KeyType[]): Promise<Configurator> => {
    const values: (string | null)[] = await this.redis.mget(
      keyTypes.map((keyType: KeyType) => `${domain}:${keyType.key}`),
    );
    if (values.some((value: string | null) => value === null))
      throw new NotFoundException(
        'At least one configuration item was not found',
      );
    return new Configurator(
      domain,
      keyTypes.map((keyType: KeyType, index: number) => ({
        domain,
        key: keyType.key,
        value: this._cast(values[index], keyType.type),
      })),
    );
  };

  set = async (domain: Domain, key: Key, value: Value): Promise<Identifier> => {
    await this.redis.set(
      `${domain}:${key}`,
      `${Array.isArray(value) ? (value.every((item) => typeof item === 'object') ? JSON.stringify(value) : value.join()) : typeof value === 'object' ? JSON.stringify(value) : value}`,
    );
    return {
      domain,
      key,
    };
  };

  private _cast = (value: string, type: Type): Value => {
    switch (type) {
      case Type.BOOLEAN:
        return value === 'true';
      case Type.INT:
        return parseInt(value);
      case Type.FLOAT:
        return parseFloat(value);
      case Type.JSON:
      case Type.JSON_ARRAY:
        return JSON.parse(value);
      case Type.INT_ARRAY:
        return value.split(',').map((item: string) => parseInt(item));
      case Type.FLOAT_ARRAY:
        return value.split(',').map((item: string) => parseFloat(item));
      case Type.STRING_ARRAY:
        return value.split(',');
      default:
        return value;
    }
  };
}
