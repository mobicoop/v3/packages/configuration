import { Provider } from '@nestjs/common';
import { CONFIGURATION_OPTIONS } from './configuration.di-tokens';
import { ConfigurationModuleAsyncOptions } from './configuration-options.interface';

export const createAsyncOptionsProvider = (
  options: ConfigurationModuleAsyncOptions,
): Provider => ({
  provide: CONFIGURATION_OPTIONS,
  useFactory: options.useFactory,
  inject: options.inject,
});
