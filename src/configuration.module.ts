import { DynamicModule, Module, Provider } from '@nestjs/common';
import { RedisModule, RedisModuleOptions } from '@songkeys/nestjs-redis';
import { CONFIGURATION_OPTIONS } from './configuration.di-tokens';
import {
  ConfigurationModuleAsyncOptions,
  ConfigurationModuleOptions,
} from './configuration-options.interface';
import { createAsyncOptionsProvider } from './configuration.provider';
import { ConfigurationRepository } from './configuration.repository';

@Module({})
export class ConfigurationModule {
  static forRootAsync(options: ConfigurationModuleAsyncOptions): DynamicModule {
    const asyncOptionsProvider: Provider = createAsyncOptionsProvider(options);

    const imports = [
      RedisModule.forRootAsync({
        inject: [CONFIGURATION_OPTIONS],
        useFactory: async (
          options: ConfigurationModuleOptions,
        ): Promise<RedisModuleOptions> => {
          return {
            config: options,
          };
        },
      }),
    ];
    return {
      global: true,
      module: ConfigurationModule,
      imports,
      providers: [asyncOptionsProvider, ConfigurationRepository],
      exports: [asyncOptionsProvider, ConfigurationRepository, RedisModule],
    };
  }
}
