import { ModuleMetadata } from '@nestjs/common';

export interface ConfigurationModuleOptions {
  host: string;
  password?: string;
  port: number;
}

export interface ConfigurationModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  useFactory?: (
    ...args: any[]
  ) => ConfigurationModuleOptions | Promise<ConfigurationModuleOptions>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  inject?: any[];
}
