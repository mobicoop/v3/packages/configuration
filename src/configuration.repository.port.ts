import { Configurator } from './configurator';

export interface GetConfigurationRepositoryPort {
  get<T>(domain: Domain, keyType: KeyType): Promise<T>;
  mget(domain: Domain, keyTypes: KeyType[]): Promise<Configurator>;
}

export interface SetConfigurationRepositoryPort {
  set(domain: Domain, key: Key, value: Value): Promise<Identifier>;
}

export type Configuration = {
  domain: Domain;
  key: Key;
  value: Value;
};

export type KeyType = {
  key: Key;
  type: Type;
};

export type Identifier = {
  domain: Domain;
  key: Key;
};

export type Key = string;
export type Value =
  | boolean
  | string
  | number
  | object
  | string[]
  | number[]
  | object[];

export enum Domain {
  AUTH = 'AUTH',
  CARPOOL = 'CARPOOL',
  GEOGRAPHY = 'GEOGRAPHY',
  MATCH = 'MATCH',
  PAGINATION = 'PAGINATION',
  HELPDESK = 'HELPDESK',
  NOTIFICATION = 'NOTIFICATION',
}

export enum Type {
  BOOLEAN = 'boolean',
  INT = 'int',
  FLOAT = 'float',
  STRING = 'string',
  STRING_ARRAY = 'string[]',
  INT_ARRAY = 'int[]',
  FLOAT_ARRAY = 'float[]',
  JSON = 'json',
  JSON_ARRAY = 'json[]',
}
