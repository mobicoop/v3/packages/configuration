import { NotFoundException } from './configuration.exception';
import { Configuration, Domain, Key } from './configuration.repository.port';

export class Configurator {
  constructor(
    public readonly domain: Domain,
    public readonly configurations: Configuration[],
  ) {}

  get = <T>(key: Key): T => {
    const configuration: Configuration | undefined = this.configurations.find(
      (configuration: Configuration) =>
        configuration.domain === this.domain && configuration.key === key,
    );
    if (configuration === undefined)
      throw new NotFoundException('Configuration item not found');
    return configuration.value as T;
  };
}
