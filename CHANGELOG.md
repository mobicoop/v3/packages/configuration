# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [8.3.0](https://gitlab.mobicoop.io/v3/packages/configuration/compare/8.2.0...8.3.0) (2024-11-28)

### Features

-   Add notification domain ([e7c1e86](https://gitlab.mobicoop.io/v3/packages/configuration/commit/e7c1e861610f755b420b969b22babb11081f0537))

## 8.2.0 (2024-09-18)

### Features

-   Add helpdesk domain ([d8282fa](https://gitlab.mobicoop.io/v3/packages/configuration/commit/d8282fad2d36160f16289043205ebcc6a97cc32b))
